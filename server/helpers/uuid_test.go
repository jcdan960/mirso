package helpers

import (
	"testing"
	guuid "github.com/google/uuid"
)

const UUIDSize = 16
const UUIDStringSize = 36

func TestUUIDSingularity(t *testing.T){
	id:= guuid.New()
	lenght := len(id.String())
	if  lenght != UUIDStringSize{
		t.Errorf("Invalid UUIDSize, received %d instead of %d", lenght, UUIDStringSize)
	}

}
package server

// inspired by:
// https://tutorialedge.net/golang/creating-restful-api-with-golang/
// and https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql
// and https://auth0.com/blog/authentication-in-golang/

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	db "mirso/server/dbManager"
	"mirso/server/users"
	"mirso/settings"
	"net/http"
)

// @title Swagger Example API
// @version 1.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /v2

var SettingsCopy settings.Settings

func homePage(w http.ResponseWriter, r *http.Request){
	log.Info("Endpoint Hit: homePage")
	fmt.Fprintf(w,"Endpoint Hit: homePage")
}

func Signin(w http.ResponseWriter, r *http.Request) {
	creds:= &users.User{}
	err := json.NewDecoder(r.Body).Decode(creds)

	if err != nil {
		//return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}

func Signup(w http.ResponseWriter, r *http.Request) {
	myUser := users.MakeUser("Maggie","salut", "maggie@woof.com")
	creds:= &users.User{}
	db.InsertUser(myUser)

	err := json.NewDecoder(r.Body).Decode(creds)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/signup", Signup)
	myRouter.HandleFunc("/signin", Signin)
	//myRouter.HandleFunc("/signin", createNewEvent).Methods("POST")
	log.Infof("Server listening on port %d", SettingsCopy.Server.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", SettingsCopy.Server.Port), myRouter))
}

func Main(set* settings.Settings) {
	SettingsCopy = *set

	db.ConnectToDB(&set.DataBase)
	handleRequests()
	db.CloseDB()
}

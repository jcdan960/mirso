package users

import (
	"github.com/google/uuid"
	"mirso/settings"
	"testing"
)

const UUIDSize = 16
const UUIDStringSize = 36

func TestForceUUID(t *testing.T){
	oldUUID := uuid.New()
	testUser := User{"Zoe","hello","zoe@gmail.com", oldUUID}

	testUser.ForceNewUUID()

	if oldUUID == testUser.UserUUID {
		t.Errorf("Invalid UUID after forcing a change")
	}
}

func TestSetUUID(t *testing.T){

	testUser := User{}
	testUser.Username = "Zoe"
	testUser.Password = "hello"
	testUser.Email ="zoe@gmail.com"

	if testUser.UserUUID != uuid.Nil{
		t.Errorf("UUID should be NIL")
	}

	testUser.SetNewUUID()

	if testUser.UserUUID == uuid.Nil{
		t.Errorf("UUID should not be nil")
	}

	newUUID := testUser.UserUUID
	testUser.SetNewUUID()

	if testUser.UserUUID != newUUID{
		t.Errorf("UUID should not have been changed")
	}

}

const testStr = "!1GolaNg5%"

func TestGetLettersCount(t *testing.T){
	letterCount := GetLettersCount(testStr)
	const expectedOutput uint16 = 6

	if  letterCount != expectedOutput {
		t.Errorf("Should have found %d letters but received %d", expectedOutput, letterCount)
	}
}

func TestGetNumberCharCount(t *testing.T){
	numberCount := GetNumberCharCount(testStr)
	const expectedOutput uint16 = 2

	if  numberCount != expectedOutput {
		t.Errorf("Should have found %d numbers but received %d", expectedOutput, numberCount)
	}

}

func TestGetSpecialCharCount(t *testing.T){

	specialCharCount := GetSpecialCharCount(testStr)
	const expectedOutput uint16 = 2

	if  specialCharCount != expectedOutput {
		t.Errorf("Should have found %d special caraters but received %d", expectedOutput, specialCharCount)
	}
}

func TestIsPasswordValid(t *testing.T){
	userTest := MakeUser("TestUser", "htwe1111234sf1", "test@test.com")

	minChar  := settings.MinimumRequiredChar{1,1,1}
	set := settings.PasswordSettings{10, minChar}

	if userTest.PasswordIsValid(&set) == nil {
		t.Errorf("Invalid password, no special char")
	}

	userTest.Password = "htwe1111234sf1$"

	if userTest.PasswordIsValid(&set) != nil {
		t.Errorf("Password is ok")
	}
}


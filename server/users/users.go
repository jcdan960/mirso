package users

import (
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"mirso/settings"
	"unicode"
)

// inspired by https://www.sohamkamani.com/blog/2018/02/25/golang-password-authentication-and-storage/

type User struct {
	Username string `json:"username`
	Password string `json:"password`
	Email string `json:"email`
	UserUUID uuid.UUID`json:"userUUID`
}

func MakeUser(Username string, Password string, Email string) *User{
	localID := uuid.New()
	localUser := User{Username, Password, Email, localID}
	return &localUser
}

func (user *User) PasswordIsValid(passwordSettings *settings.PasswordSettings) error{
	if uint16(len(user.Password)) < passwordSettings.MinimumLength {
		return fmt.Errorf("password must have a minimum length of %d, but received %d", passwordSettings.MinimumLength, len(user.Password))
	}

	minimums := passwordSettings.MinimumRequiredChar

	if GetLettersCount(user.Password) < minimums.Letters {
		return fmt.Errorf("password must have a minimum number of letters of %d, but received %d", minimums.Letters, len(user.Password))
	}

	if GetNumberCharCount(user.Password) < minimums.Numbers{
		return fmt.Errorf("password must have a minimum number of Numbers of %d, but received %d", minimums.Letters, len(user.Password))
	}

	if GetSpecialCharCount(user.Password) < minimums.SpecialChar{
		return fmt.Errorf("password must have a minimum number of SpecialsChar of %d, but received %d", minimums.Letters, len(user.Password))
	}

	return nil
}

func GetLettersCount(s string) uint16 {
	var counter uint16 = 0
	for _, r := range s {
		if unicode.IsLetter(r) {
			counter += 1
		}
	}
	return counter
}

func GetNumberCharCount(s string) uint16 {
	var counter uint16 = 0
	for _, r := range s {
		if unicode.IsNumber(r) {
			counter += 1
		}
	}
	return counter
}

func GetSpecialCharCount(s string) uint16 {
	var counter uint16 = 0
	for _, r := range s {
		if IsSpecialCharacter(r) {
			counter += 1
		}
	}
	return counter
}

func IsSpecialCharacter(r rune) bool{
	return (r >= '!' && r < '0') ||
		(r >= ':' && r < 'A') ||
		(r > 'Z' && r < 'a' ) ||
		(r > 'z'&& r <= '~' )
}

func (u *User) SetNewUUID(){
	if u.UserUUID == uuid.Nil{
		u.UserUUID = uuid.New()
		log.Infof("Setting new UUID to user %s", u.Username)
	}else{
		log.Infof("Cannot set new UUID to user %s UUID already exists", u.Username)
	}
}

func (u *User) ForceNewUUID(){
	log.Infof("Setting new UUID to user %s", u.Username)
	u.UserUUID = uuid.New()
}
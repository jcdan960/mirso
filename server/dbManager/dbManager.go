package dbManager

import (
	"context"
	"github.com/jackc/pgx/v4"
	log "github.com/sirupsen/logrus"
	"fmt"
	"mirso/server/users"
	"mirso/settings"
)

var DB *pgx.Conn

func CloseDB(){
	DB.Close(context.Background())
}

func ConnectToDB(set *settings.DatabaseSettings) {

	log.Info("Connecting to db...")
	psqlInfo := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", set.User, set.Password, set.Host, set.Port,  set.DBName)
	log.Info(psqlInfo)

	conn, err := pgx.Connect(context.Background(), psqlInfo)
	if err != nil {
		log.Fatalf("Error connecting to DB, %s", err)
		panic(err)
	}

	DB = conn
	log.Info("Successfully connected to Database")
}

func InsertUser(user * users.User){
	log.Infof("Inserting user %s", user.Username)

	query := fmt.Sprintf(`insert into USERS(username, password, email, uuid) values('%s', '%s', '%s', '%s');`, user.Username, user.Password, user.Email, user.UserUUID)

	_,err :=  DB.Exec(context.Background(), query)

	if err != nil {
		log.Error(err)
	}
}
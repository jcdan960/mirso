package main

import (
	log "github.com/sirupsen/logrus"
	"mirso/server"
	"mirso/settings"
)

func main() {

	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)

	log.Info("System booting up")
	settings := settings.ReadSettings("settings/settings.json")

	if settings == nil {
		return
	}

	server.Main(settings)
}

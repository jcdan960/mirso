# Requirements
- Go >= 1.16
    

# Installation and Setting up on Linux
```go get "github.com/Sirupsen/logrus" ```

```go get "github.com/gorilla/mux" ``` 

```go get -u github.com/lib/pq ```

```go get -u github.com/swaggo/swag/cmd/swag ```

``` go get github.com/google/uuid ```

``` go get github.com/jackc/pgx/v4 ```


# Generate API doc with Swagger
``` swag init  -g server/server.go ```

# PostGreSQL commands:
```su postgres```
```psql```

to list DB
``` \list```

to connect to DB
```\connect mirso```

To show tables in DB
```\dt```

To Create Table:
``` CREATE TABLE USERS(username VARCHAR(50) NOT NULL, password VARCHAR(50) NOT NULL, email VARCHAR(200) UNIQUE NOT NULL, UUID VARCHAR(36) UNIQUE NOT NULL);```

To grant privileges:
```GRANT ALL PRIVILEGES ON DATABASE mirso TO db_manager;```
```GRANT ALL PRIVILEGES ON ALL TABLES in SCHEMA public TO db_manager;```


To insert User:
```insert into USERS(username, password, email, uuid) values ('JC','yo','jc@gmail.com', 'dsdssdssdssdsd');```
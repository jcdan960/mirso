package settings

import (
	"io/ioutil"
	log "github.com/sirupsen/logrus"
	"encoding/json"
)

type DatabaseSettings struct {
	Host string `json:"Host"`
	Port uint16 `json:"Port"`
	User string `json:"User"`
	Password string `json:"Password"`
	DBName string `json:"DBName"`
	Tablename string `json:"Tablename"`
}

type ServerSettings struct {
	Port uint16 `json:"Port"`
}

type PasswordSettings struct {
	MinimumLength uint16 `json:"MinimumLength"`
	MinimumRequiredChar MinimumRequiredChar `json:"MinimumRequiredChar"`
}

type MinimumRequiredChar struct{
	Numbers uint16 `json:"Numbers"`
	Letters uint16 `json:"Letters"`
	SpecialChar uint16 `json:"SpecialChar"`
}

type UsersSettings struct {
	Password PasswordSettings `json:"Password"`
}

type Settings struct{
	Server ServerSettings `json:"Server"`
	DataBase DatabaseSettings `json:"DataBase"`
	Users UsersSettings `json:"Users"`
}

func ReadSettings(settingsFile string) *Settings {

	file, err := ioutil.ReadFile(settingsFile)

	if err != nil {
		log.Error("Error loading settings file")
		log.Error(err)
		return nil
	}

	settings := Settings{}
	_ = json.Unmarshal([]byte(file), &settings)

	return &settings
}

